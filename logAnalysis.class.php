<?php
require_once('analysisUtil.class.php');
class logAnalysis extends analysisUtil
{
	public $st_time = '';
	public $ed_time = ''; 
	public $result = array();
	public $record_lines = 0;
	protected $file_path = '';
	protected $startable = false;

	public function __construct($file_path){
		$this->file_path = $file_path;
		$this->checkFileValid();
		//init run start time
		$this->st_time = time();
	}
	public function start(){
		if(!$this->startable)return;
		$file_contents = @file($this->file_path);
		if(false === $file_contents || !is_array($file_contents)) throw new Exception("parse file error", 1);
		if(empty($file_contents)) return;//empty log content
		//var_dump($file_contents);
		foreach ($file_contents as $line => $content) {
			$this->record_lines += 1;
			//echo $content;
			preg_match('/^(\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3})\s-\s(.*)\s\[(.*)\]\s\"(.*)\"\s(\d{3,})\s(\d+)\s(.*)\s\"([^\s]*)\"\s\"(.*?)\"\s\"(.*)\"$/iu', $content, $matches);
			//var_dump($matches);
			try {
				$this->result[$line]['remote_addr'] = $matches[1];
				$this->result[$line]['remote_user'] = trim($matches[2], '-');
				$this->result[$line]['time_local'] = strtotime($matches[3]);
				$this->result[$line]['request'] = $this->parseRequest($matches[4]);
				$this->result[$line]['status'] = trim($matches[5], '-');
				$this->result[$line]['body_bytes_sent'] = trim($matches[6], '-');
				$this->result[$line]['request_body'] = trim($matches[7], '-');
				$this->result[$line]['http_referer'] = trim($matches[8], '-');
				$this->result[$line]['http_user_agent'] = $this->parseUserAgent(trim($matches[9], '-'));
				$this->result[$line]['http_x_forwarded_for'] = trim($matches[10], '-');
			} catch (Exception $e) {
						
			}		
		}
		$this->ed_time = time();
	}
	private function parseRequest($request){
		//get request url
		preg_match('/\s(.*)\s/i', $request, $matches);
		$req_url = isset($matches[1]) ? $matches[1] : '';

		preg_match('/\?ch_id=(\d{1,})&/iu', $req_url, $matches);//匹配微信公众平台post的url中的ch_id
		$ch_id = isset($matches[1]) ? $matches[1] : '';
	
		preg_match('/\?id=(\d{1,})_(\d{1,})/iu', $req_url, $matches);//匹配访问 大转盘页面的传递的参数
		$act_id = isset($matches[1]) ? $matches[1] : '';
		$u_id = isset($matches[2]) ? $matches[2] : '';
		return array(
			'req_url' => $req_url,
			'ch_id' => $ch_id,
			'u_id' => $u_id,
			'act_id' => $act_id
		);
	}
	private function parseUserAgent($agent){
		preg_match('/\s\(([^\(]*)\)\s.*\s([^\/].*)$/i', $agent, $matches);
		$opr_system = isset($matches[1]) ? $matches[1] : '';
		$browser = isset($matches[2]) ? $matches[2] : '';
		return array(
			'req_system' => $opr_system,
			'req_browser' => $browser
		);
	}
	private function checkFileValid(){
		if (!file_exists($this->file_path)) {
			//throw new Exception("the file is not exists! file path is " . $this->file_path, 1);
			$this->log('the file is not exists! file path is ' . $this->file_path);
		}
		if (!is_readable($this->file_path)) {
			//throw new Exception("the file is not readable!", 1);
			$this->log('the file is not readable! file path is ' . $this->file_path);
		}
		$this->startable = true;
	}
	public function log($log_str){
		if(!TRACK_ERROR_LOG) return;
		$log_name = 'nginx_analysis_' . date('Ymd') . '.log';
		error_log(date('Y-m-d H:i:s') . "\t" . $log_str . " \r\n", 3, LOG_ROOTPATH . $log_name);	
	}
}