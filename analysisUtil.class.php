<?php
class analysisUtil
{
	public $ip_taobao = 'http://ip.taobao.com/service/getIpInfo.php?ip=';

    public function ip_location($ip)
    {
		$data = $this->curl($this->ip_taobao . $ip);
		$dataObj = json_decode($data, true);
		if(is_array($dataObj) && $dataObj['code'] == 0){
			$ip_info = $dataObj['data'];
			return $ip_info;
		}else{
			throw new Exception("get ip information error", 1);
		}
    }
    public function curl($url, $postFields = null)
    {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_FAILONERROR, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		//https 请求
		if(strlen($url) > 5 && strtolower(substr($url,0,5)) == "https" ) {
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		}

		if (is_array($postFields) && 0 < count($postFields)) {
			$postBodyString = "";
			$postMultipart = false;
			foreach ($postFields as $k => $v) {
				if("@" != substr($v, 0, 1)) {//判断是不是文件上传
				
					$postBodyString .= "$k=" . urlencode($v) . "&"; 
				}
				else {//文件上传用multipart/form-data，否则用www-form-urlencoded
				
					$postMultipart = true;
				}
			}
			unset($k, $v);
			curl_setopt($ch, CURLOPT_POST, true);
			if ($postMultipart)
			{
				curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
			}
			else
			{
				curl_setopt($ch, CURLOPT_POSTFIELDS, substr($postBodyString,0,-1));
			}
		}
		$reponse = curl_exec($ch);
		
		if (curl_errno($ch))
		{
			throw new Exception(curl_error($ch),0);
		}
		else
		{
			$httpStatusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			if (200 !== $httpStatusCode)
			{
				throw new Exception($reponse,$httpStatusCode);
			}
		}
		curl_close($ch);
		return $reponse;
    }
}
