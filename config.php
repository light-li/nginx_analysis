<?php
/** PHP 系统异常配置 */
ini_set('display_errors', 'on');
//set_time_limit(0);
error_reporting(E_ALL);
/*database*/
define('DB_HOST','127.0.0.1');
define('DB_USER','root');
define('DB_PWD','');
define('DATA_BASE','***');

/*log*/
define('TRACK_ERROR_LOG', true);
define('LOG_ROOTPATH','D:\Program Files\wamp\www');
/*nignx log*/
define('NGINX_LOG_PATH', 'D:\Program Files\wamp\www');

/*debug*/
define('DEBUG', false);
